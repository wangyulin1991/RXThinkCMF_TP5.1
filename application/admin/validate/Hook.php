<?php

namespace app\admin\validate;

use app\common\validate\BaseValidate;

/**
 * 钩子-验证类
 * @author 牧羊人
 * @date 2019/6/27
 * Class Hook
 * @package app\admin\validate
 */
class Hook extends BaseValidate
{

}
